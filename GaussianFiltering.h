/*
 * GaussianFiltering.h
 *
 *  Created on: May 14, 2014
 *      Author: hogan
 */

#ifndef GAUSSIANFILTERING_H_
#define GAUSSIANFILTERING_H_

void GaussianFiltering3D_double(double*, double*, int*, double, double);
void GaussianFiltering2D_double(double*, double*, int*, double, double);

#endif /* GAUSSIANFILTERING_H_ */
