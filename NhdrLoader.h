/*
 * NhdrLoader.h
 *
 *  Created on: Nov 15, 2013
 *      Author: hogan
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <fstream>

using namespace std;

#ifndef NHDRLOADER_H_
#define NHDRLOADER_H_

extern int sizes[3];
extern string file;

//read the nhdr file to get the info on the raw file
void readNhdr(char* myFile, int VolumeSizes[]);

//split the header data for info gathering
void splitString(string myString, int VolumeSizes[]);

//read the raw file and save the data into the vector data
void readRaw(const char *myFile, vector<short>& data);

#endif /* NHDRLOADER_H_ */
