/*
 * PeeksMapsAnalysis.h
 *
 *  Created on: May 14, 2014
 *      Author: hogan
 */

//#include <vector>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

#ifndef PEEKSMAPSANALYSIS_H_
#define PEEKSMAPSANALYSIS_H_


std::vector<std::pair<int,int> > gaussianAndPeeksAnalysis(std::vector<std::vector<int> >, int);


#endif /* PEEKSMAPSANALYSIS_H_ */
