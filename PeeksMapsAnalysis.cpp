/*
 * PeeksMapsAnalysis.cpp
 *
 *  Created on: May 14, 2014
 *      Author: hogan
 */

#include "GaussianFiltering.h"
#include <vector>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <fstream>
#include <utility>
#include <sstream>
#include <map>
#include <list>

//void GaussianFiltering3D_double(double *I, double *J, int *dimsI, double sigma, double kernel_size)
//void GaussianFiltering2D_double(double *I, double *J, int *dimsI, double sigma, double kernel_size)

using namespace std;

map<int, pair<int,int> > connectedComponentLabeling(vector<vector<double> >, int);

vector<double> gaussedLineMap(90*360);
vector<double> doublePeeksMap(90*360);

//find the highest value in a region
pair<double,pair<int,int> >  highestValueInCluster(vector<pair<int,int > > cluster, vector<vector<double> > values){
    double highest = 0;
    pair<int,int> coord (0,0);
    for(unsigned int i=0;i<cluster.size();i++){
        if(values[cluster[i].first][cluster[i].second]>highest){
            highest = values[cluster[i].first][cluster[i].second];
            coord.first=cluster[i].first;
            coord.second=cluster[i].second;
        }
    }
    pair<double,pair<int,int> > returnValue (highest,coord);
    return returnValue;
}

pair<map<int,vector<pair<int,int> > >, map<int, pair<int,int> > > peekClusters(vector<vector<int> > labels, int indexRegion, vector<vector<double> > peeksMap){
    //the int key of the map is the region label, and it contains a vector of coordinate pairs referring to all of its elements
    map<int,vector<pair<int,int> > > peeksClusters;
    map<int, pair<int,int> > peeksPerClusterRegion;

    for(unsigned int i=0;i<labels.size();i++){
        for(unsigned int j=0;j<labels[0].size();j++){
            //if the map doesn't contain yet elements with the given label
            if(peeksClusters.find(labels[i][j])==peeksClusters.end()){
                vector<pair<int,int> > newListCoords(0);
                peeksClusters[labels[i][j]]=newListCoords;
                peeksClusters[labels[i][j]].push_back(pair<int,int>(i,j));
            }else{
                peeksClusters[labels[i][j]].push_back(pair<int,int>(i,j));
            }
        }
    }

    typedef map<int,vector<pair<int,int > > >::iterator pCIterator;
//    cout << "Region : " << indexRegion << endl;
//    peeksClusters.erase(0);
    pair<double, pair<int,int> > highestPeek;
    vector<int> toBeRemoved;

    //Clean the clusters by removing small ones and those with too low highest values
    for(pCIterator iterator = peeksClusters.begin(); iterator != peeksClusters.end(); iterator++){
        int keyValue = iterator->first;
        vector<pair<int,int > > tempVec = iterator->second;
        highestPeek=highestValueInCluster(tempVec,peeksMap);
        //store the highestpeek coordinates for the label
        peeksPerClusterRegion[keyValue]=highestPeek.second;
        if((tempVec.size()<50 || highestPeek.first<9.0)){
            //only save the index of the ones to be removed to avoid causing problems to the iterator
            toBeRemoved.push_back(keyValue);
        }
    }

    //remove the elements to be removed and their highest peeks as well
    for(unsigned int i = 0;i<toBeRemoved.size();i++){
        peeksClusters.erase(toBeRemoved[i]);
        peeksPerClusterRegion.erase(toBeRemoved[i]);
    }

    for(pCIterator iterator = peeksClusters.begin(); iterator != peeksClusters.end(); iterator++){
        //int keyValue = iterator->first;
        vector<pair<int,int > > tempVec = iterator->second;
//        cout << "Label : " << keyValue << ", Elements : " << tempVec.size() << endl;
    }

    pair<map<int,vector<pair<int,int> > >, map<int, pair<int,int> > > totalReturn;
    totalReturn.first=peeksClusters;
    totalReturn.second=peeksPerClusterRegion;

    return totalReturn;

}



void smoothPeeksMap(vector<double> peeksMap){
    int dimsI[2] = {90,360};
    GaussianFiltering2D_double(&peeksMap[0], &gaussedLineMap[0], dimsI, 2.0, 12.0);
}

void saveToFile(int indexRegion){
    int tetaSize = 90;
    int phiSize = 360;

    ofstream outFile;
    ostringstream mapName;
    mapName << "../PeeksMaps/Region" << indexRegion << "map" << tetaSize << "x" << phiSize <<"_gauss.txt";
    outFile.open(mapName.str().c_str());
    for(int i=0;i<tetaSize;i++){
        for(int j=0;j<phiSize;j++){
            outFile << gaussedLineMap[i+j*90] << " " ;
        }
        outFile << "\n";
    }
    outFile.close();
}

vector<vector<double> >  reconstructMatrix(vector<double> map){
    vector<vector<double> > reconstructedGaussedMap(90, vector<double>(360));

    int k = 0;

    for(int i=0;i<90;i++){
        for(int j=0;j<360;j++,k++){
            reconstructedGaussedMap[i][j] = map[i+j*90];
        }
    }
    return reconstructedGaussedMap;
}

vector<pair<int,int> > gaussianAndPeeksAnalysis(vector<vector<int> > peeksMap, int indexRegion){

    gaussedLineMap.reserve(90*360);
    doublePeeksMap.reserve(90*360);

    int k = 0;

    for(int i=0;i<360;i++){
        for(int j=0;j<90;j++,k++){
                doublePeeksMap[k]=(double)peeksMap[j][i];
        }
    }

    smoothPeeksMap(doublePeeksMap);

    saveToFile(indexRegion);

    map<int, pair<int,int> > peeks;

    //TODO change return of connected component labeling and imgaussian so to return the peeks of the region only
    peeks = connectedComponentLabeling(reconstructMatrix(gaussedLineMap),indexRegion);

    vector<pair<int,int> > peeksCoords;

    typedef map<int, pair<int,int> >::iterator peeksIt;
    for(peeksIt iterator = peeks.begin(); iterator != peeks.end(); iterator++){
        peeksCoords.push_back(iterator->second);
    }

    if(peeks.size()==0){
        pair<int,int> emptyPeek;
        emptyPeek.first = -1;
        emptyPeek.second = -1;
        peeksCoords.push_back(emptyPeek);
    }


//    return reconstructMatrix(gaussedLineMap);
    return peeksCoords;
}

vector<vector<int> > clusterLabels(90, vector<int>(360));
vector<vector<double> > smoothedPeeksMap;

const int dx[] = {+1, 0, -1, 0};
const int dy[] = {0, +1, 0, -1};

void dfs(int x, int y, int current_label) {
  if (x < 0 || x == 90) return; // out of bounds
  if (y < 0 || y == 360) return; // out of bounds
  if (clusterLabels[x][y] || (smoothedPeeksMap[x][y]<7)) return; // already labeled or not marked with 1 in m
  // mark the current cell
  clusterLabels[x][y] = current_label;
  // recursively mark the neighbors
  for (int direction = 0; direction < 4; ++direction)
    dfs(x + dx[direction], y + dy[direction], current_label);
}

//method to label the connected component (clusters) in the map
map<int, pair<int,int> > connectedComponentLabeling(vector<vector<double> > peeksMap, int indexRegion){
    for(int i=0;i<90;i++){
        for(int j=0;j<360;j++){
                clusterLabels[i][j]=0;
        }
    }
    smoothedPeeksMap = peeksMap;
    int component = 0;
    for (int i = 0; i < 90; ++i){
        for (int j = 0; j < 360; ++j){
//            if (!clusterLabels[i][j] && (smoothedPeeksMap[i][j]>=1.3)) dfs(i, j, ++component);
            if (!clusterLabels[i][j] && (smoothedPeeksMap[i][j]>=7)) dfs(i, j, ++component);

        }
    }

    pair<map<int,vector<pair<int,int> > >, map<int, pair<int,int> > > totalReturn;

    //all the regions of the map indexed in the map through their index
    map<int,vector<pair<int,int> > > cleanedClusters;
    map<int, pair<int,int> > peeksPerClusters;

    totalReturn = peekClusters(clusterLabels, indexRegion, smoothedPeeksMap);

    cleanedClusters = totalReturn.first;
    peeksPerClusters = totalReturn.second;

//    cout << "Number of peeks " << peeksPerClusters.size() << endl;

    return peeksPerClusters;
//    if(indexRegion==36){
//        typedef map<int,pair<int,int > >::iterator pCIterator;
//        for(pCIterator iterator = peeksPerClusters.begin(); iterator != peeksPerClusters.end(); iterator++){
//            cout << " PEEK : " << iterator->first << " - " << iterator->second.first <<";" << iterator->second.second << endl;
//        }
//    }

//    ofstream outFile;
//    ostringstream mapName;
//    mapName << "../PeeksMaps/RegionLabels" << indexRegion << "map" << 90 << "x" << 360 <<"_gauss.txt";
//    outFile.open(mapName.str().c_str());
//    for(int i=0;i<90;i++){
//        for(int j=0;j<360;j++){
//            outFile << clusterLabels[i][j] << " " ;
//        }
//        outFile << "\n";
//    }
//    outFile.close();
}
