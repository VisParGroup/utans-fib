/*
 * VtkWriter.cpp
 *
 *  Created on: Jan 30, 2014
 *      Author: hogan
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <fstream>
#include </usr/include/eigen3/Eigen/Dense>
#include "/usr/include/eigen3/Eigen/Eigenvalues"
#include <utility>
#include "VtkWriter.h"

using namespace Eigen;
using namespace std;

string fileVersion = "# vtk DataFile Version 3.0\n";
string header = "SFRC Vector Alignment data\n";
string format = "ASCII\n";
string datatype1 = "DATASET UNSTRUCTURED_GRID\n";
string datatype2 = "POINTS ";
string datatype3 = "ORIGIN ";
string datatype4 = "SPACING ";

typedef pair<double[3],Vector3d> coordVector;

//struct peekAnalysisData{
//    int regionIndex;
//    int clusteringLevel;
//    float centroid[3];
//    vector<Vector3d> alignVectors;
//};

///TODO SHIFT TO THE 0 THE DATASET

void writeVtkRegionPeeks(vector<peekAnalysisData> data, const char* fileName){

    vector<int> clusterSizes(0);
    for(int i=0;i<data.size();i++){
        clusterSizes.push_back(data[i].alignVectors.size());
    }

    ofstream outFile;
    outFile.open(fileName);
    outFile << fileVersion;
    outFile << "Single Region Multiple peeks data vectors \n";
    outFile << format;
    outFile << datatype1;
    outFile << datatype2  << data.size() << " DOUBLE\n";
    for(int i=0;i<data.size();i++){
        outFile << "" << data[i].centroid[2] << " " << data[i].centroid[1] << " "<< data[i].centroid[0] << "\n";
    }
    outFile << "\n";
    outFile << "CELLS " << data.size() << " " << data.size()*2 << "\n";
    for(int i=0;i<data.size();i++){
        outFile <<"1 " << i << "\n";
    }
//    for(int i=0;i<data.size();i++){
//        outFile << data[i].alignVectors.size() << " " << i << "\n";
//    }
//    for(int i=0;i<clusterSizes.size();i++){
//        outFile << clusterSizes[i] << " " ;
//        for(int j=0;j<clusterSizes[i];j++){
//            outFile << i << " ";
//        }
//        outFile << "\n";
//    }
    outFile << "\n";
    outFile << "CELL_TYPES " << data.size() << "\n";
    for(int i=0;i<data.size();i++){
        outFile << "1 " << "\n";
    }
    outFile << "\n";
    outFile << "POINT_DATA " << data.size() << "\n";

    outFile << "\n";
    outFile << "SCALARS clustering DOUBLE 1\n";
    outFile << "LOOKUP_TABLE default\n";
    for(int i=0;i<data.size();i++){
            outFile << "" << i << "\n";
    }
//    for(int i=0;i<clusterSizes.size();i++){
//        for(int j=0;j<clusterSizes[i];j++){
//            outFile << i << "\n";
//        }
//    }

    outFile << "\n";

    for(int i=0;i<clusterSizes.size();i++){
        outFile << "VECTORS alignmentvectors" << i << " DOUBLE\n";
                for(int j=0;j<data[i].alignVectors.size();j++){
                    outFile << "" << data[i].alignVectors[j](2) << " " << data[i].alignVectors[j](1)  << " "<< data[i].alignVectors[j](0) << "\n";
                }
                //outFile << "" << centroids[i].second(2) << " " << centroids[i].second(1)  << " "<< centroids[i].second(0) << "\n";
                outFile << "\n";
    }

//    outFile << "VECTORS alignmentvectors DOUBLE\n";
//    for(int i=0;i<data.size();i++){
//        for(int j=0;j<data[i].alignVectors.size();j++){
//            outFile << "" << data[i].alignVectors[j](2) << " " << data[i].alignVectors[j](1)  << " "<< data[i].alignVectors[j](0) << "\n";
//        }
//        //outFile << "" << centroids[i].second(2) << " " << centroids[i].second(1)  << " "<< centroids[i].second(0) << "\n";
//    }


    outFile << "\n";
    outFile.close();

    cout << "VTK regionPeeksAnalysis FILE SAVED" << endl;

}

void writeVtkAlignFile(vector<pair<Vector3d, Vector3d > >& centroids, const char* fileName){
    ofstream outFile;
    outFile.open(fileName);
    outFile << fileVersion;
    outFile << "SFRC Labeled Vector Alignment data\n";
    outFile << format;
    outFile << datatype1;
    outFile << datatype2  << centroids.size() << " DOUBLE\n";

    for(int i=0;i<centroids.size();i++){
            outFile << "" << centroids[i].first[2] << " " << centroids[i].first[1] << " "<< centroids[i].first[0] << "\n";
//            outFile << "" << centroids[i].first[0] << " " << centroids[i].first[1] << " "<< centroids[i].first[2] << "\n";
    }

    outFile << "\n";
    outFile << "CELLS " << centroids.size() << " " << centroids.size()*2 << "\n";
    for(int i=0;i<centroids.size();i++){
        outFile <<"1 " << i << "\n";
    }
    outFile << "\n";
    outFile << "CELL_TYPES " << centroids.size() << "\n";
    for(int i=0;i<centroids.size();i++){
            outFile << "1 " << "\n";
    }

    outFile << "\n";
    outFile << "POINT_DATA " << centroids.size() << "\n";

    outFile << "\n";

    outFile << "SCALARS region DOUBLE 1\n";
    outFile << "LOOKUP_TABLE default\n";
    for(int i=0;i<centroids.size();i++){
            outFile << "" << i << "\n";
    }

            outFile << "\n";

    outFile << "VECTORS alignmentvectors DOUBLE\n";
    for(int i=0;i<centroids.size();i++){
        //THIS IS THE WAY TO PRINT THE FIBRES VECTOR COHERENT WITH THE DATA AXES (+,-,+)
            outFile << "" << centroids[i].second(0) << " " << -centroids[i].second(1)  << " "<< centroids[i].second(2) << "\n";
    }


    outFile << "\n";

    outFile.close();

    cout << "VTK FILE SAVED" << endl;
}

void writeVtkFile(vector<coordVector>& data, vector<double>& eigenvalues, vector<double>& graylevel, int sizeX, int sizeY, int sizeZ, const char* filename){
    int startX, startY, startZ;
    int endX, endY, endZ;

    string line;

    ofstream outFile;
    outFile.open(filename);

    outFile << fileVersion;
    outFile << header;
    outFile << format;
    outFile << datatype1;
    outFile << datatype2  << data.size() << " FLOAT\n";

    for(int i=0;i<data.size();i++){
        outFile << "" << (data[i].first[2]) << " " << (data[i].first[1]) << " "<< (data[i].first[0]) << "\n";
    }
    outFile << "\n";
    outFile << "CELLS " << data.size() << " " << data.size()*2 << "\n";
    for(int i=0;i<data.size();i++){
            outFile <<"1 " << i << "\n";
    }
    outFile << "\n";
    outFile << "CELL_TYPES " << data.size() << "\n";
    for(int i=0;i<data.size();i++){
                outFile << "1 " << "\n";
        }

    outFile << "\n";
    outFile << "POINT_DATA " << data.size() << "\n";

    outFile << "SCALARS eigenvalues DOUBLE 1\n";
    outFile << "LOOKUP_TABLE default\n";
    for(int i=0;i<eigenvalues.size();i++){
            outFile << "" << eigenvalues[i] << "\n";
        }

    outFile << "\n";

    outFile << "SCALARS grayvalues DOUBLE 1\n";
    outFile << "LOOKUP_TABLE default\n";
    for(int i=0;i<graylevel.size();i++){
            outFile << "" << graylevel[i] << "\n";
    }

        outFile << "\n";

    outFile << "VECTORS alignmentvectors FLOAT\n";
    for(int i=0;i<data.size();i++){
            outFile << "" << (data[i].second(2)) << " " << (data[i].second(1))  << " "<< (data[i].second(0)) << "\n";
        }


    outFile << "\n";

    outFile.close();
}



