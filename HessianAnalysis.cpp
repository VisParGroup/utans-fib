/*
 * HessianAnalysis.cpp
 *
 *  Created on: Nov 15, 2013
 *      Author: (c) 2013 Emiliano Pastorelli, Institute of Cybernetics, Tallinn University of Technology, Tallinn, Estonia
 */

#include "NhdrLoader.h"
#include "VtkWriter.h"
#include "PeeksMapsAnalysis.h"

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include </usr/include/eigen3/Eigen/Dense>
#include "/usr/include/eigen3/Eigen/Eigenvalues"
#include <list>
#include <ctime>
#include <vector>
#include <string>
#include <fstream>
#include <math.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <iostream>

//#include "Lights.h"
//#include <GL/glut.h>
//#include <GL/gl.h>
//#include "Camera.h"
//#include "VisualizationTool.h"

using namespace Eigen;
using namespace std;

//simple structure to save an int[3] array into a vector (otherwise not possible in c++)
struct Index3 {
    short i[3];
};

// TODO insert class to store all variables
class CFibreVisu
{
public:
    //_________members
    //sizes of the volume data
    int sizes[3];

    //---vectors
    // data read in from file
    vector<short> myVec;
    //3D-array used to store the volume data
    vector<vector<vector<double> > > volume;
    //vector to save the data with spatial information to be saved in the vtk file format
    vector<coordVector> saveVector;
    // vector to save gray levels of data set in
    vector<double> saveGrayLevel;
    // vector to store the eigenvalues of the data set in
    vector<double> saveEigenvalues;
    //vector used as a mask to decide the voxels to be analyzed and the region to which they belong. obtained by the labelmax from the filtering
    vector<vector<vector<int> > > volumeMask;

    //couple of vectors to store the vector for each region and the coordinates for the same
    vector<vector<Vector3d> > VectorByLabels;
    vector<vector<Vector3d> > CoordByLabels;
    vector<Vector3d> centroids;
    vector<pair<int,int> > peeks;
    vector<Vector3d> peeksAvgCoordinates;

    vector<pair<Vector3d,Vector3d > > saveVectorsLabels;
    //vector of vector of coordinates representing the indices of each point belonging to different regions
    vector<vector<Index3> > regionIndices;
    //3D-array used to store the volume cleaned data
    vector<vector<vector<double> > > volumeClean;


    //---bools
    ///TODO comment!
    bool saveToVTK;    ///< flag whether to save data to VTK file

    // these variables are used to remove core artifact of the scan, currently commented
    // bool  GrayLevels;
    // bool GrayLevelsClusters;

    //starting x,y and z position of the loaded dataset
    // (necessary to save it centered in vtk later and to match points, vector and regions positions)
    short starterValues[3];
    int xSize;        ///< size of dataset along x-direction
    int ySize;        ///< size of dataset along x-direction
    int zSize;         ///< size of dataset along z-direction

    // this whole storing of files is a little bit crazy --> get rid of it

    string volumeName;
    string volumeSection;
    string volumeCompleteName;
    string pathVolume;
    string subFolderPath;
    //ofstream to save the xyz values of all the vectors
    ofstream saveVectorsOnly;

    //Eigen structures to store hessian matrices, calculate eigenvalues and vectors
    Matrix3d hessian;                        ///< hessian matrix for the each pixel of the data
    Matrix3d V3outerV3;                        ///< outer product of smallest eigenvector with itself
    Matrix3d TotalResult;                    ///< total orientation tensor
    vector<Matrix3d> AverageProductsVolume; ///< store all averages of second order tensors

    //variables to contain the derivatives and mixed derivatives for building the Hessian matrix of each pixel
    float fxx;
    float fyy;
    float fzz;
    float fxy;
    float fxz;
    float fyz;

    //additional thresholding of the dataset
    int minThreshold,maxThreshold;         ///< minimal and maximal value of threshold
    //subvolumes number (if all set to 1 -> no subvolumes used)
    int subVolumesZ;
    int subVolumesY;
    int subVolumesX;
    // sizes of the subvolumes to take into consideration
    int sizeSubX, sizeSubY, sizeSubZ;
    int restSubX, restSubY, restSubZ;

    //to store the values dropped from the outlier removal (at the moment non in use)
    // variable is not even used TODO get rid of it?
    int droppedElements;

    //variables used to time the execution
    clock_t start;
    clock_t finish;
    clock_t begin;
    clock_t end;
    double elapsed_secs;

    //--- functions
    void allocateVolume();                                                                 ///< allocate 3D volume array
    void volumeBuilder(/*vector<short> myVec*/);                                         ///< build 3D volume array
    void splitString(string myString, short start[]);                                     ///< splits a string into 3 short arrays
    void generateSlice(int x, int y, int z);                                             ///< generate test data slices TODO remove
    void generateRealFibre(int x, int y, int z);                                         ///< TODO comment
    void generateFibre(int x, int y, int z, int sizeOnX, int sizeOnY, int sizeOnZ, int id); ///< TODO comment
    void generateExample(); ///< TODO comment
    bool neigThreshold(double max, double min, int x, int y, int z);                     ///< check if value is within threshold
    void HesseAnalysis(int subX, int subY, int subZ, int maxX, int maxY, int maxZ);        ///< Hesse analysis on subvolume
    void HesseBuilder ();
    void LabelMapReader(vector<vector<Index3> >& labelIndices, string fileName, short start[]);///< full analysis depending on subvolume
    void IndexInRegion(vector<vector<Index3> > myRegions);
    void calculateVectorsPeek(vector<Vector3d> region, vector<Vector3d> regionCoordinates,   int indexRegion, int clustering, int outputClustering);
    void buildVTKLabeled(int clustering);
    void calculateSizeSubvolumes();

};

//typedef to save in a single vector the coordinates of a point and its corresponding orientation vector
typedef pair<double[3],Vector3d> coordVector;


//**********************************MIGHT BE USELESS NOW**********************************//
vector<vector<vector<int> > > clusterLayersHighestPeeks(0);
vector<peekAnalysisData> saveMultiplePeeks(0);

//allocate a mask volume to fill with the region labels
 // void CFibreVisu::allocateMaskVolume(){
//    //int myxSize = sizes[0];
//    //int myySize = sizes[1];
//    //int myzSize = sizes[2];
//    vector<int> zV(sizes[0]);
//    vector<vector<int> > yV (sizes[1], zV);
//    vector<vector<vector<int> > > xV(sizes[2], yV);
//    volumeMask=xV;

//    cout << "Allocated mask volume : " << sizes[0] << "|" << sizes[1]<< "|" << sizes[2] << endl;
//}

//given a string of 3 short values separated by comma, split it into a short[3] array
void CFibreVisu::splitString(string myString, short start[]){
  size_t found1,found2,found3;
  found1=myString.find(',');
  start[0]=atoi(myString.substr(0,found1).c_str());
  found2=myString.find(',',found1+1);
  start[1]=atoi(myString.substr(found1+1,found2-1).c_str());
  found3=myString.find(',',found2+1);
  start[2]=atoi(myString.substr(found2+1,found3-1).c_str());
}

//read the .lri file, gather and build the indices for each region
void CFibreVisu::LabelMapReader(vector<vector<Index3> >& labelIndices, string fileName, short start[]){

    //--------------------------------------------
    // variables
    //--------------------------------------------
    ifstream inFile;            ///< file that is opened
    string line;                ///< file is read in line by line --> current line is stored in this variable
    short sX,sY,sZ;
    double centrTemp[3];        ///< temporary storage of centroids
    Vector3d newVec;            ///< vector for storage of centroids (only used in VTK file --> no correction)
    vector<Index3> temp;        ///< temporary storage for labelindices
    size_t found1,found2,found3; ///< storage for position where label indices is found in


    // open file
    inFile.open(fileName.c_str());

    cout << "Loading label region data from file : " << fileName << endl;



    if(inFile.is_open()){
        // read in first line
        getline(inFile,line);
        cout << line << endl;
        // read in second line (starter values)
        getline(inFile,line);
        // divide into starter values
        splitString(line.substr(17,line.length()-1),start);
//        cout << "Starter Values " << start[0]<<","<<start[1]<<","<<start[2] << endl;
        int i = 0;
        while(getline(inFile,line)){
            // if the word Region is found within the line
            if(line.find("Region ")!= string::npos){
                if(atoi(line.substr(7,line.length()-1).c_str())==0){
                    temp.clear();
                    //cout << "REGION 0" << endl;
                    i++;
                }
                else{
                    // only store if region not equal to 0
                    labelIndices.push_back(temp);
                    temp.clear();

                    //cout << "REGION " << i << endl;
                    i++;
                }
            }
            // if the word centroid is found
            else if(line.find("Centroid ")!= string::npos){

                //---------------
                // store all three centroids
                //--------------
                string indLine = line.substr(10,line.length()-2);
                size_t found1,found2,found3;
                found1=line.find(',');
                centrTemp[0]=atof(line.substr(10,found1).c_str());
                found2=line.find(',',found1+1);
                centrTemp[1]=atof(line.substr(found1+2,found2-1).c_str());
                found3=line.find(',',found2+1);
                centrTemp[2]=atof(line.substr(found2+2,found3-1).c_str());

                //the centroids are not used in the software analysis (only for saving the vtk results) and therefore are not corrected
                newVec << centrTemp[0],centrTemp[1],centrTemp[2];
                centroids.push_back(newVec);
            }
            /// if the end of the file is reached
            else if(line.find("EndOfFile")!= string::npos){
                // store label indices permanently
                labelIndices.push_back(temp);
                // close file
                inFile.close();
                break;
            }
            /// indices of label points?
            else if(line.find("[")!= string::npos){
                string indLine = line.substr(1,line.length()-2);

                  found1=line.find(',');
                  sX=atoi(line.substr(1,found1).c_str());
                  found2=line.find(',',found1+1);
                  sY=atoi(line.substr(found1+2,found2-1).c_str());
                  found3=line.find(',',found2+1);
                  sZ=atoi(line.substr(found2+2,found3-1).c_str());
                  Index3 newI3;
                  //NOPE//here the indices of the labels points are converted from x,y,z to z,y,x TODO this todo is only here to point to that line
                  newI3={{sX,sY,sZ}};

                  // store the label indices into temp --> that way the label indices get stores right behind the regions itself (at least that is the only way this architecture makes sense=
                  temp.push_back(newI3);


            }
        }
    }

    //allocate one empty vector for each used region
    for (unsigned int i=0;i<labelIndices.size();i++){
        vector<Vector3d> tempRegion(0);
        VectorByLabels.push_back(tempRegion);
        CoordByLabels.push_back(tempRegion);
    }

    cout << "Starting Values : " << starterValues[0] << " , " << starterValues[1] << " , " << starterValues[2] << endl;
    cout << "Number of regions : " << labelIndices.size() << endl;
}

//method that, given the structure holding the indices of all the ordered regions. fills up the volumeMask 3d array
void CFibreVisu::IndexInRegion(vector<vector<Index3> > myRegions){
    //for each region
    for(unsigned int i=0;i<myRegions.size();i++){
        //foreach coordinates triplet in region
        int test = 0;
        for(unsigned int j=0;j<myRegions[i].size();j++){
            /// subtract startervalue to store it centered in vtk later
            int x = myRegions[i][j].i[2]-starterValues[2];
            int y = myRegions[i][j].i[1]-starterValues[1];
            int z = myRegions[i][j].i[0]-starterValues[0];
//            volumeMask[x][y][z]=(i+1);

            //if then to drill out the core of the dataset
            //TODO magic numbers, what are these numbers?
            if((y>390 && y<410) && (z>390 && z<410)){
                volumeMask[x][y][z]=(0);
                test++;
            }else{
                volumeMask[x][y][z]=(i+1);

            }
        }
//        cout << "Region " << (i+1) << " large " << myRegions[i].size() << " - " << test << endl;
    }
}

//dynamically allocate the volume array
void CFibreVisu::allocateVolume( ){
//    xSize = sizes[0];
//Size = sizes[1];
//Size = sizes[2];
    vector<double> zVec( sizes[0]);
    vector<vector<double> > yVec ( sizes[1], zVec);
    vector<vector<vector<double> > > xVec(sizes[2], yVec);
    volume = xVec;
}

//remove the core (rectangular section only) from the analyzed dataset
//void core(int mX, int MX, int mY, int MY, int mZ, int MZ){
//    for(int i=mZ;i<MZ;i++){
//        for(int j=mY;j<MY;j++){
//            for(int t=mX;t<MX;t++){
//                volume[i][j][t]=19000;
//            }
//        }
//    }
//}

//dynamically build the 3D volume array using the data read from the raw file
void CFibreVisu::volumeBuilder(/*vector<short> myVec*/){
    for (int i=0;i<zSize;i++){
        for (int j=0;j<ySize;j++){
            for (int t=0;t<xSize;t++){
                double val=(myVec[i*ySize*xSize+j*xSize+t]);
                volume[i][j][t]=val;
            }
        }
    }




//REMOVE THE CORE ARTIFACT OF THE SCAN
//    core(370,422,369,439,0,576);

//    if(GrayLevels || GrayLevelsClusters){
//        ostringstream name;
//        for (int i=0;i<zSize;i++){
//            for (int j=0;j<ySize;j++){
//                for (int t=0;t<xSize;t++){
//                    extractGrayLevels(volume[i][j][t]);
//                }
//            }
//        }
//        if(GrayLevels){
//            saveGrayLevels("./fibre_test.GL");
//        }else{
//            clusterGrayLevels(10, "./fibre_cluster_test.GL");
//        }
//    }
}

//METHODS TO GENERATE TEST SAMPLE DATASETS TODO REMOVE THEM SOON
void CFibreVisu::generateSlice(int x, int y, int z){
    for (int i=0;i<3;i++){
        for (int j=0;j<3;j++){
            volume[z+i][y][x+j]=22000;
            if(y==599){
//            cout << "x,y,z : " << x+j << "," << y << "," << z+i << ";" << endl ;
            }
        }
    }
}
void CFibreVisu::generateRealFibre(int x, int y, int z){
    int vertI = 0;
    for (int i=0;i<200;i++){
        if(i==41){vertI+=1;}
        if(i==81){vertI+=1;}
        if(i==121){vertI+=1;}
        if(i>140 && i<160){vertI+=1;}

        generateSlice(x,y+i,z+vertI);
    }

    vertI = 0;
    for (int i=0;i<200;i++){
        if(i==41){vertI+=1;}
        if(i==81){vertI+=1;}
        if(i==121){vertI+=1;}
        if(i>140 && i<160){vertI+=1;}

        generateSlice(x,y-i,z+vertI);
    }
}
void CFibreVisu::generateFibre(int x, int y, int z, int sizeOnX, int sizeOnY, int sizeOnZ, int id){
    for (int i=z;i<z+sizeOnZ;i++){
        for (int j=y;j<y+sizeOnY;j++){
            for (int t=x;t<x+sizeOnX;t++){
                if(id==0){
                    volume[i][j][t]=22000;
                }
                if(id==1){
                    volume[i][j][t]=22001;
                }
                if(id==2){
                    volume[i][j][t]=22002;
                }
            }
        }
    }
}
void CFibreVisu::generateExample(){
    for (int i=0;i<zSize;i++){
        for (int j=0;j<ySize;j++){
            for (int t=0;t<xSize;t++){
                volume[i][j][t]=0;
            }
        }
    }

    generateRealFibre(400,400,200);

//    generateFibre(400,200,200,4,400,4,0);
//    generateFibre(430,200,0,8,8,576,0);
//    generateFibre(150,420,0,8,8,576,0);

//    generateFibre(0,420,273,795,1,1,1);
//    generateFibre(0,50,230,795,8,8,1);
//    generateFibre(0,500,280,795,8,8,1);
//
//    generateFibre(350,0,320,8,788,8,2);
//    generateFibre(300,0,210,8,788,8,2);
//    generateFibre(450,0,410,8,788,8,2);

}

//return the index of the eigenvector corresponding to the smaller eigenvalue
int minorEigenvalue(Vector3d eigenval){
    int ind = 0;
    for (int j=1; j<3; j++){
        if(abs(double(eigenval(j)))<=(abs(double(eigenval(j-1))))){
            ind = j;
        }
    }
    return ind;
}

//check if a values falls into the given threshold
bool CFibreVisu::neigThreshold(double max, double min, int x, int y, int z){
    if((double)(volume[x][y][z]<max) && (double)(volume[x][y][z]>min)){
        return true;
    }
    return false;
}

//return the sign of a float number (1 or -1)
int sign(double val){
    if(val>0) return 1;
    if(val<0) return -1;
    return 0;
}

//given a region converts all the belonging vectors to polar coordinates and calculates the alignment peek
void CFibreVisu::calculateVectorsPeek(vector<Vector3d> region, vector<Vector3d> regionCoordinates,   int indexRegion, int clustering, int outputClustering){

    vector<Vector3d> peak(0);
    vector<vector<Vector3d> > phiCoords(360,peak);
    vector<vector<vector<Vector3d> > > coordinatesPerCluster(90,phiCoords);

    //variable to
    int cS =clustering;

    int tetaSize = (int)90/cS;
    //TODO the values of Phi should range between 0 and 360, therefore having a size of 361 should be excluded
    int phiSize = (int)360/cS;

    //*********OPTIMAL WAY TO INIT A MULTIDIMENTIONAL VECTOR TO NOT INCUR IN MEMORY CORRUPTION PROBLEMS*********//
    vector<vector<int> > coordinateClusters(tetaSize,vector<int>(phiSize));

    //vector for the polar coordinates of the whole region
    /// first: theta, second: phi
    vector<pair<int,int> > polarCoordinates;

    for(int i=0;i<tetaSize;i++){
        for(int j=0;j<phiSize;j++){
                coordinateClusters[i][j]=0;
        }
    }

    //go through the region and get the polar coordinates of each voxel/vector of the region
    for(unsigned int i=0;i<region.size();i++){
        //for each vector element of the region extract the polar coordinates
        pair<int,int> newPolar;

        double x,y,z;        ///< store negativevoxel coordianates

        Vector3d newVec = region[i];

        //CORRECT - Heiko said it's not... ??? TODO CHECK for heavens sake!
        x = (double)(-newVec(2));
        y = (double)(-newVec(1));
        z = (double)(-newVec(0));

        saveVectorsOnly << x << " " << y << " " << z << "\n";


        //convert the vector angles from cartesian to spherical polar coordinates
        newPolar.first = (int)round(atan2(sqrt(1-z*z),z)*180.0/M_PI);
        if(x>0){
            // calculate angle within x/y-plane and than convert from degree to radian
            newPolar.second = (int)round(atan2(y,x)*180.0/M_PI);
        }
        else{
            /// x is very close to 0
            if(x<0.00000000001 && x>-0.00000000001){
                /// multiply with half circle and convert to radian
                newPolar.second = (int)round(sign(y)*M_PI/2.0*180.0/M_PI);
            }
            else {
            // Case not needed, because both lines are the same    if (x<0 && y>=0){
                    newPolar.second = (int)round(atan2(y,x)*180.0/M_PI);
            //    }else{
            //        newPolar.second = (int)round(atan2(y,x)*180.0/M_PI);
            //    }
            }
        }

        ///---------------------------------------------------------
        //force them into a range of 0-90 for Teta and 0-360 for Phi
        ///---------------------------------------------------------
        if(newPolar.first>90){
            newPolar.first=180-newPolar.first;
            newPolar.second=newPolar.second+180;
        }
        newPolar.second=(newPolar.second+360)%360;

        //save the vector coordinates in the coordinates array(vector)
        polarCoordinates.push_back(newPolar);
    }

    int indexA;
    int indexB;
    //cluster all the values in the 2d matrix map
    for(unsigned int i = 0 ; i<polarCoordinates.size() ; i++){
        //use this control to make sure that no indices are outside the ranges
        if((int)(polarCoordinates[i].first/cS)>=0){
            if((int)(polarCoordinates[i].first/cS)<tetaSize){
                indexA = (int)(polarCoordinates[i].first/cS);
            }else{
                indexA = tetaSize-1;
            }
        }else {
            indexA = 0;
        }
        if((int)(polarCoordinates[i].second/cS)>=0){
            if((int)(polarCoordinates[i].second/cS)>=phiSize){
                indexB = phiSize-1;
            }else{
                indexB = (int)(polarCoordinates[i].second/cS);
            }
        }else {
            indexB = 0;
        }
/// TODO check again what happens there?
        coordinatesPerCluster[indexA][indexB].push_back(regionCoordinates[i]);

        //increase the amount of elements stored in that cell
        coordinateClusters[indexA][indexB]++;
    }

    //for each cell of the thetaphi map store a vector cointaining the coordinates
    vector<vector<Vector3d> > averagePositions(tetaSize,vector<Vector3d>(phiSize));

    Vector3d sum;

    //TODO optimize by summing up already on the first calculation and then only averaging now
    for(unsigned int i=0;i<coordinatesPerCluster.size();i++){
        for(unsigned int j=0;j<coordinatesPerCluster[0].size();j++){

            if(coordinatesPerCluster[i][j].size()>10){
                sum << 0,0,0;
                for(unsigned int t=0;t<coordinatesPerCluster[i][j].size();t++){
                    sum[0]+=coordinatesPerCluster[i][j][t][0];
                    sum[1]+=coordinatesPerCluster[i][j][t][1];
                    sum[2]+=coordinatesPerCluster[i][j][t][2];
                }
                //calculate the average depending on the amount of points
                sum/=coordinatesPerCluster[i][j].size();
                //push the averaged vector at the end of the vector of coordinates for that cell
                averagePositions[i][j]=sum;
            }
            /// don't take too small clusters into consideration
            else{
                sum << 0,0,0;
                averagePositions[i][j]=sum;
            }
        }
    }

    vector<pair<int,int> > regionPeaks = gaussianAndPeeksAnalysis(coordinateClusters,indexRegion);

    for(unsigned int i = 0;i<regionPeaks.size();i++){
        //If the peak is valid, store its coordinates (plus the starting value of the dataset)
        if(regionPeaks[i].first!=-1){
            Vector3d coordinateOfSinglePeek;
            peeksAvgCoordinates.push_back(averagePositions[regionPeaks[i].first][regionPeaks[i].second]);
            peeksAvgCoordinates.back()[0]+=starterValues[2];
            peeksAvgCoordinates.back()[1]+=starterValues[1];
            peeksAvgCoordinates.back()[2]+=starterValues[0];
            peeks.push_back(regionPeaks[i]);
        }else{
            //TODO EVALUATE IF NECESSARY
            Vector3d nullVec;
            nullVec << 0.0,0.0,0.0;
            peeksAvgCoordinates.push_back(nullVec);
            peeks.push_back(pair<int,int>(-1,-1));
        }
    }
}

//build the structures to save the vtk file of the simplified vectors for the peeks
void CFibreVisu::buildVTKLabeled(int clustering){
    double x,y,z;
    //reconstruct values according to clustering
    int cS = clustering;
    float cSDim = clustering/2;

    for(unsigned int i=0;i<peeks.size();i++){
        if(peeks[i].first==-1){
            // TODO not valid peak, figure out further investigation?
        }else{
            //rebuild the cartesian coordinates from the spherical coordinates of the given peek
            x=sin(((peeks[i].first)*cS+cSDim)*M_PI/180.0)*cos(((peeks[i].second)*cS+cSDim)*M_PI/180.0);
            y=sin(((peeks[i].first)*cS+cSDim)*M_PI/180.0)*sin(((peeks[i].second)*cS+cSDim)*M_PI/180.0);
            z=cos(((peeks[i].first)*cS+cSDim)*M_PI/180.0);

            Vector3d alignVec;
            alignVec << x,y,z;

            //save together the directional vector of a peak and its centroid
            pair<Vector3d, Vector3d> newPair;
            newPair.second = alignVec;
            newPair.first = peeksAvgCoordinates[i];
            saveVectorsLabels.push_back(newPair);
        }
    }

    string saveName;
    saveName = subFolderPath+"/"+volumeCompleteName+"_Fibres_Orientation-Vectors.vtk";

    writeVtkAlignFile(saveVectorsLabels,saveName.c_str());

    string outFileName = subFolderPath+"/"+"peaksThetaPhi-"+volumeCompleteName+".txt";
    ofstream outFile;
    outFile.open(outFileName.c_str());
    outFile << "theta phi\n";
    for(unsigned int i=0;i<peeks.size();i++){
        if(peeks[i].first!=(-1) && peeks[i].second!=(-1)){
            outFile << peeks[i].first << " " << peeks[i].second << "\n";
        }
    }
    outFile.close();
}

//perform analysis on a subvolume (give the overlapping +2 subvolume and check for the corner side values externally)
void CFibreVisu::HesseAnalysis(int subX, int subY, int subZ, int maxX, int maxY, int maxZ){

    //------------------------------------------------------------------------
    //  problem: at the moment we have two angles for every fibre, but we want a distributional function
    //    solution: calculate second order tensor (outer product of smallest eigenvalue), take average of them as distribution
    //------------------------------------------------------------------------


    vector<Matrix3d> OuterVector;        ///< storage for all outer products (second order tensor)
    Matrix3d AverageProduct(3,3);        ///< distribution of the fibres

    //init all to zero otherwise eigen goes mad
    AverageProduct << 0,0,0,0,0,0,0,0,0;

    int counter=0;                         ///< counts number of voxels belonging to fibre

    //THIS TRY IS A MISTERY, BUT NECESSARY
    try{
        /// iterate through all 3D data
        for (int x=subX;x<(maxX);x++){
            for (int y=subY;y<(maxY);y++){
                for (int z=subZ;z<(maxZ);z++){

                    //filter the voxels of interest depending on the mask extracted from the labels and additionaly on a threshold on the graylevels
                    if(neigThreshold(maxThreshold,minThreshold,x,y,z) && (int)volumeMask[x][y][z]!=0){

//                        if((int)volumeMask[x][y][z]==55){

                        ///--------------------------------------------------------
                        /// calculate matrix for each voxel belonging to fibre
                        ///--------------------------------------------------------

                        /// determine derivatives for hessian matrix
                            fxx=((volume[x+2][y][z]-volume[x][y][z])/2.0-(volume[x][y][z]-volume[x-2][y][z])/2.0)/2.0;
                            fyy=((volume[x][y+2][z]-volume[x][y][z])/2.0-(volume[x][y][z]-volume[x][y-2][z])/2.0)/2.0;
                            fzz=((volume[x][y][z+2]-volume[x][y][z])/2.0-(volume[x][y][z]-volume[x][y][z-2])/2.0)/2.0;
                            //build partial mixed derivatives for the pixel
                            fxy=((volume[x+1][y+1][z]-volume[x+1][y-1][z])/2.0-(volume[x-1][y+1][z]-volume[x-1][y-1][z])/2.0)/2.0;
                            fxz=((volume[x+1][y][z+1]-volume[x+1][y][z-1])/2.0-(volume[x-1][y][z+1]-volume[x-1][y][z-1])/2.0)/2.0;
                            fyz=((volume[x][y+1][z+1]-volume[x][y+1][z-1])/2.0-(volume[x][y-1][z+1]-volume[x][y-1][z-1])/2.0)/2.0;

                            //compose hessian matrix for the pixel, remember that fxy=fyz TODO why is that the same?
                            hessian <<     fxx, fxy, fxz,
                                        fxy, fyy, fyz,
                                        fxz, fyz, fzz;

                            ///--------------------------------------------------------
                            ///  store smallest eigenvalue
                            ///--------------------------------------------------------
                            //extract eigenvalues and choose the eigenvector related to the smallest eigenvalue,
                            //and do the outer product of it with itself
                            SelfAdjointEigenSolver<Matrix3d> solver(hessian);
                            int minorEigen = minorEigenvalue(solver.eigenvalues().real());

                            //obtain the eigenvector of the smaller eigenvalue
                            Vector3d v3 =solver.eigenvectors().col(minorEigen).real();

                            // store the value of the minor Eigen into variable
                            const double& MinorEigenValue = solver.eigenvalues().real()[minorEigen];

                            /// outer product of smallest eigenvector with itself (also called autocorrelationmatrix)
                            V3outerV3 = v3*v3.transpose();

                            //save the labels in the label regions they belong to
                                VectorByLabels[(int)volumeMask[x][y][z]-1].push_back(v3);

                                /// store voxel coordinates and
                                Vector3d voxelCoordinates;
                                voxelCoordinates << x,y,z;
                                CoordByLabels[(int)volumeMask[x][y][z]-1].push_back(voxelCoordinates);

                                int indexCurrentRegion = (int)volumeMask[x][y][z]-1;

                                bool allDataset = true;
                                int interestingRegion = 7;

                                /// want to save to VTK?
                                if(saveToVTK ){
                                    if(!allDataset){ /// all dataset
                                        if(indexCurrentRegion==interestingRegion){
                                            coordVector newElem;
                                            newElem.first[0]=x+starterValues[2];
                                            newElem.first[1]=y+starterValues[1];
                                            newElem.first[2]=z+starterValues[0];
                                            newElem.second = v3;
                                            saveVector.push_back(newElem);
                                            saveGrayLevel.push_back(volume[x][y][z]);
                                            saveEigenvalues.push_back(MinorEigenValue);
                                //            saveEigenvalues.push_back(solver.eigenvalues().real()[minorEigen]);
                                        }
                                    }
                                    /// partial data set
                                    else{
                                        coordVector newElem;
                                        newElem.first[0]=x+starterValues[2];
                                        newElem.first[1]=y+starterValues[1];
                                        newElem.first[2]=z+starterValues[0];
                                        newElem.second = v3;
                                        saveVector.push_back(newElem);
                                        saveGrayLevel.push_back(volume[x][y][z]);
                                        saveEigenvalues.push_back(MinorEigenValue);
                                    //saveEigenvalues.push_back(solver.eigenvalues().real()[minorEigen]);
                                    }
                                }
                            /// save all outer vectors
                            OuterVector.push_back(V3outerV3);
                            /// increment counter of
                            counter++;
//                        }
                    }
                }
            }
        }
    }
    /// catch exception
    catch(std::bad_alloc& ba){
            cerr << "Bad alloc : " << ba.what() << '\n';
            cout << "After : " << counter << " elements" << endl;
    }


//    cout << "Elements analyzed : " << counter << endl;

    int countRec = 0;         ///< counter for summed elements

    ///-------------------------------------------
    /// build average of all second order tensors
    ///-------------------------------------------
    /// for every outer outer product
    for(int i = 0;i<(int)OuterVector.size();i++){
        AverageProduct+=OuterVector[i];
        countRec++;
    }

//    cout << "Elements summed : " << countRec << endl;

//    cout << "Subvolume sum : " << endl;
//    cout << AverageProduct << endl;

    AverageProduct = AverageProduct/countRec;

//    cout << "SIZE : " << OuterVector.size() << endl;

    cout << "Subvolume average : " << endl;
    cout << AverageProduct << endl;

    /// store into vector of average products
    if(countRec>0){
        AverageProductsVolume.push_back(AverageProduct);
    }

//    cout << "Kept " << i << " out of " << j << endl;

    // magic number: TODO erase?
    int clusteringSize = 1;

    //save the vectors for each voxel to file
    string saveVectorsFileName = subFolderPath+"/"+volumeCompleteName+"_Vectors.xyz";
    saveVectorsOnly.open(saveVectorsFileName.c_str());
    saveVectorsOnly << "x y z\n";

    ///-------------------------------------------------------------------------------------------
    //for each region convert into polar coordinates, cluster on the surface map, calculate peeks
    ///-------------------------------------------------------------------------------------------
    for(unsigned int i = 0;i<VectorByLabels.size();i++){
        calculateVectorsPeek(VectorByLabels[i], CoordByLabels[i],i,1,clusteringSize); //TODO magic number, not necessary?
    }
    saveVectorsOnly.close();

    buildVTKLabeled(clusteringSize);
}

//method to manage the full analysis depending on subvolumes
void CFibreVisu::HesseBuilder (){

    int startValX, stopValX;        ///< stopping and starting point of iteration in trough volume X-direction  (depends on number of subvolumes)
    int startValY, stopValY;        ///< stopping and starting point of iteration in trough volume Y-direction  (depends on number of subvolumes)
    int startValZ, stopValZ;        ///< stopping and starting point of iteration in trough volume Z-direction  (depends on number of subvolumes)

    // for each subvolume
    for (int subZ = 0;subZ <subVolumesZ ; subZ++){
        for (int subY = 0;subY <subVolumesY ; subY++){
            for (int subX = 0;subX <subVolumesX ; subX++){

                // init start variables TODO why not init before loop?
                if(subZ==0){
                    startValZ=2;
                }else{ startValZ=0; }
                if(subY==0){
                    startValY=2;
                }else{ startValY=0; }
                if(subX==0){
                    startValX=2;
                }else{ startValX=0; }

                /// set stop volume according to number of subvolumes
                //e.g. subVolume = 1 --> stopVal = 1 - 2 + size (size % 1) = size - 1
                if(subZ==subVolumesZ-1){
                    stopValZ=sizeSubZ-2+restSubZ;
                }else{ stopValZ=sizeSubZ+2; }
                if(subY==subVolumesY-1){
                    stopValY=sizeSubY-2+restSubY;
                }else{ stopValY=sizeSubY+2; }
                if(subX==subVolumesX-1){
                    stopValX=sizeSubX-2+restSubX;
                }else{ stopValX=sizeSubX+2; }

                clock_t begin = clock();

                cout << "Starting with SubVolume : " << endl;
                cout << subZ << "," << subY << "," << subX << endl;
                cout << "That starts from " << startValZ+(subZ*sizeSubZ) << "," << startValY+(subY*sizeSubY) << "," << startValX+(subX*sizeSubX) << endl;
                cout << "and goes until " << stopValZ+(subZ*sizeSubZ) << "," << stopValY+(subY*sizeSubY) << "," << stopValX+(subX*sizeSubX) << endl;


                /// Hesse analysis
                HesseAnalysis(startValZ+(subZ*sizeSubZ), startValY+(subY*sizeSubY), startValX+(subX*sizeSubX), stopValZ+(subZ*sizeSubZ), stopValY+(subY*sizeSubY), stopValX+(subX*sizeSubX));
                cout << "Subvolume calculated" << endl;
                clock_t end = clock();
                double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;

                cout << "Time taken : " << elapsed_secs << " seconds" << endl;
            }
        }
    }
}

//support function to calculate the precise size of the subvolumes used
void CFibreVisu::calculateSizeSubvolumes(){
    sizeSubZ = floor(zSize/subVolumesZ);
    restSubZ = zSize%subVolumesZ;
    sizeSubY = floor(ySize/subVolumesY);
    restSubY = ySize%subVolumesY;
    sizeSubX = floor(xSize/subVolumesX);
    restSubX = xSize%subVolumesX;

}

int main(int argc, char* argv[]){
    //example of software arguments - older version with lots of arguments only
    /*
     * 4b_full_filtered.nhdr     #.nhdr header of volume to analyze
     * 30                          #min threshold
     * 20000                     #max threshold
     * -C                         #(-C)Compute/Visualize(-V)/Computer&Visualize(-VC) - OpenGL rough visualization is purely for initial debugging
     * 1                        #subvolumesX
     * 1                        #subvolumesY
     * 1                        #subvolumesZ
     * -VTK                        #save to vtk
     * -GL                        #(-GL)Gray Levels/(-GLC)Gray levels clusters (slow)
     */

    // instanciate class
    CFibreVisu FibreVisu;

    time_t timer;
    time(&timer);
    /// set variables that were former given at startup --> variable candidates for GUI
    string debugVis = "-V";
    string debugCalcVis = "-CV";

    string VTK = "-VTK";
    string GL = "-GL";
    string GLCLUSTERS = "-GLC";

    // TODO maybe even delete this variables. only used in commented function
    //FibreVisu.GrayLevelsClusters = false;
    //FibreVisu.GrayLevels = false;
    FibreVisu.saveToVTK = true;          // init of flag --> per default save VTK file

    /// TODO comment
   FibreVisu.minThreshold = 30;            ///< minimal value of threshold
   FibreVisu.maxThreshold = 20000;         ///< maximal value for threshold
   /// subvolume to be processed. If all is set to 1 --> no subvolume used
   FibreVisu.subVolumesX = 1;
   FibreVisu.subVolumesY = 1;
   FibreVisu.subVolumesZ = 1;

    // TODO rearrange Filename(s)

    //disassemble the first argument and obtain path, file name and section
    unsigned pathLast = string(argv[1]).find_last_of("/");
    //isolate the path of the folder in which the volume is stored
    FibreVisu.pathVolume =  string(argv[1]).substr(0,pathLast);
    //isolate the volume header file name
    string volumeHeaderFileName = string(argv[1]).substr(pathLast+1,string(argv[1]).length());
    unsigned found = volumeHeaderFileName.find_first_of("-");
    //save the volume name (e.g. 5a)
    FibreVisu.volumeName = volumeHeaderFileName.substr(0,found);
    unsigned found_section = volumeHeaderFileName.substr(found+1,string(argv[1]).length()).find_first_of("_");
    //save the volume section (e.g. top)
    FibreVisu.volumeSection = volumeHeaderFileName.substr(found+1,found_section);
    //full volume name (e.g. 5a-top)
    FibreVisu.volumeCompleteName = FibreVisu.volumeName+"-"+FibreVisu.volumeSection;

    //load the labels file from the same folder the volume is stored in
    string fileIndices = FibreVisu.pathVolume+"/"+"regionLabels_"+ FibreVisu.volumeCompleteName+".lri";
    FibreVisu.LabelMapReader(FibreVisu.regionIndices,fileIndices,FibreVisu.starterValues);
    cout << "Labels loaded from " << fileIndices << endl;

    FibreVisu.subFolderPath = FibreVisu.pathVolume+"/"+ FibreVisu.volumeCompleteName+"_Analysis";

    //check if the subfolder exists, otherwise create it
    struct stat st = {0};
    if (stat(FibreVisu.subFolderPath.c_str(), &st) == -1) {
        mkdir(FibreVisu.subFolderPath.c_str(), 0700);
        cout << "Subfolder successfully created " << endl;
    }
    cout<< "Subfolder successfully loaded" << endl;
    


    cout << "*** Volumetric SFRC Analysis Software ***" << endl;
    cout << "Selected volume file : " << argv[1] << endl;
    cout << "Volume sample being analyzed : " << FibreVisu.volumeCompleteName << endl;

    if(FibreVisu.saveToVTK){
        cout << "The extracted vector data, gray values and eigenvalues will be saved to VTK file" << endl;
    }

//    start = clock();

    // read in ndhr input file
    readNhdr(argv[1], FibreVisu.sizes);
    cout << "Nhdr Loaded - The software will now load the raw file" << endl;

    //----------------------------------------------
    // allocate a mask volume to fill with the region labels
    // mask volume = volume of data set
    //---------------------------------------------
        vector<int> zV(FibreVisu.sizes[0]);
        vector<vector<int> > yV (FibreVisu.sizes[1], zV);
        vector<vector<vector<int> > > xV(FibreVisu.sizes[2], yV);
        FibreVisu.volumeMask=xV;

        cout << "Allocated mask volume : " << FibreVisu.sizes[0] << "|" << FibreVisu.sizes[1]<< "|" << FibreVisu.sizes[2] << endl;

    // fill up maskvolume
    FibreVisu.IndexInRegion(FibreVisu.regionIndices);


    /// begin counter
    FibreVisu.begin = clock();

    //load volume from raw file
    string rawFilePath = FibreVisu.pathVolume +"/"+file;
    cout << "Full path of Raw file being loaded : " << rawFilePath << endl;
    readRaw(rawFilePath.c_str(),FibreVisu.myVec);

    cout << "The software will now allocate and build the data structures" << endl;
//    begin = clock();

    //--------------------------------------------------
    /// allocate volume needed according to data size
    //  that is the name of the pseudo function  FibreVisu.allocateVolume();
    // -------------------------------------------------

    vector<double> zVec( FibreVisu.sizes[0]);
    vector<vector<double> > yVec ( FibreVisu.sizes[1], zVec);
    vector<vector<vector<double> > > xVec(FibreVisu.sizes[2], yVec);
    FibreVisu.volume = xVec;



    //---------------------------------------
    /// build volume
    // name of former pseudo function: FibreVisu.volumeBuilder();
    //---------------------------------------
    /// iterate through the whole volume voxel by voxel
    for (int i=0;i<FibreVisu.zSize;i++){
                for (int j=0;j<FibreVisu.ySize;j++){
                    for (int t=0;t<FibreVisu.xSize;t++){
                        // assign right volume data to each voxel
                        double val=(FibreVisu.myVec[i*FibreVisu.ySize*FibreVisu.xSize+j*FibreVisu.xSize+t]);
                        // store volume data in voxel
                        FibreVisu.volume[i][j][t]=val;
                    }
                }
            }


//    end = clock();
//    elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
//    cout << "Volume correctly built in : " << elapsed_secs << " seconds" << endl;

    //----------------------------------------------------
    /// calculate the size of specified subvolume
    // name of old pseudo function: FibreVisu.calculateSizeSubvolumes();
    //----------------------------------------------------
    FibreVisu.sizeSubZ = floor(FibreVisu.zSize/FibreVisu.subVolumesZ);
    FibreVisu.restSubZ = FibreVisu.zSize%FibreVisu.subVolumesZ;
    FibreVisu.sizeSubY = floor(FibreVisu.ySize/FibreVisu.subVolumesY);
    FibreVisu.restSubY = FibreVisu.ySize%FibreVisu.subVolumesY;
    FibreVisu.sizeSubX = floor(FibreVisu.xSize/FibreVisu.subVolumesX);
    FibreVisu.restSubX = FibreVisu.xSize%FibreVisu.subVolumesX;



    //----------------------------------------------------
    /// Do the bulk of the operations
    // name of old pseudo function: FibreVisu.HesseBuilder;
    //----------------------------------------------------
    int startValX, stopValX;        ///< stopping and starting point of iteration in trough volume X-direction  (depends on number of subvolumes)
    int startValY, stopValY;        ///< stopping and starting point of iteration in trough volume Y-direction  (depends on number of subvolumes)
    int startValZ, stopValZ;        ///< stopping and starting point of iteration in trough volume Z-direction  (depends on number of subvolumes)

    // iterate through each subvolume
    for (int subZ = 0;subZ <FibreVisu.subVolumesZ ; subZ++){
        for (int subY = 0;subY <FibreVisu.subVolumesY ; subY++){
            for (int subX = 0;subX <FibreVisu.subVolumesX ; subX++){

                // init start variables TODO why not init before loop?
                if(subZ==0){
                    startValZ=2;
                }else{ startValZ=0; }
                if(subY==0){
                    startValY=2;
                }else{ startValY=0; }
                if(subX==0){
                    startValX=2;
                }else{ startValX=0; }

                /// set stop volume according to number of subvolumes
                //e.g. subVolume = 1 --> stopVal = 1 - 2 + size (size % 1) = size - 1
                if(subZ==FibreVisu.subVolumesZ-1){
                    stopValZ=FibreVisu.sizeSubZ-2+FibreVisu.restSubZ;
                }else{ stopValZ=FibreVisu.sizeSubZ+2; }
                if(subY==FibreVisu.subVolumesY-1){
                    stopValY=FibreVisu.sizeSubY-2+FibreVisu.restSubY;
                }else{ stopValY=FibreVisu.sizeSubY+2; }
                if(subX==FibreVisu.subVolumesX-1){
                    stopValX=FibreVisu.sizeSubX-2+FibreVisu.restSubX;
                }else{ stopValX=FibreVisu.sizeSubX+2; }

                clock_t begin = clock();

                cout << "Starting with SubVolume : " << endl;
                cout << subZ << "," << subY << "," << subX << endl;
                cout << "That starts from " << startValZ+(subZ*FibreVisu.sizeSubZ) << "," << startValY+(subY*FibreVisu.sizeSubY) << "," << startValX+(subX*FibreVisu.sizeSubX) << endl;
                cout << "and goes until " << stopValZ+(subZ*FibreVisu.sizeSubZ) << "," << stopValY+(subY*FibreVisu.sizeSubY) << "," << stopValX+(subX*FibreVisu.sizeSubX) << endl;


                /// Hesse analysis
                FibreVisu.HesseAnalysis(startValZ+(subZ*FibreVisu.sizeSubZ), startValY+(subY*FibreVisu.sizeSubY), startValX+(subX*FibreVisu.sizeSubX), stopValZ+(subZ*FibreVisu.sizeSubZ), stopValY+(subY*FibreVisu.sizeSubY), stopValX+(subX*FibreVisu.sizeSubX));
                cout << "Subvolume calculated" << endl;
                clock_t end = clock();
                double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;

                cout << "Time taken : " << elapsed_secs << " seconds" << endl;
            }
        }
    }

    //-.-------------------------------------------------
    // build average of volume
    // TODO: averageproductvolume is only filled once. can we get rid of taking the average?
    //---------------------------------------------------
    for(int i = 0;i<(int)FibreVisu.AverageProductsVolume.size();i++){
        FibreVisu.TotalResult += FibreVisu.AverageProductsVolume[i];
    }
    FibreVisu.TotalResult = FibreVisu.TotalResult/FibreVisu.AverageProductsVolume.size();


    cout << "Total Orientation Tensor : " << endl;
    cout << FibreVisu.TotalResult << endl;

    cout << "Total amount of voxels belonging to fibres : " << FibreVisu.saveVector.size() << endl;

    ostringstream name;
    name << FibreVisu.subFolderPath << "/" << FibreVisu.volumeCompleteName << "_Voxel-Vectors.vtk";

    cout << "Saving as " << name.str() << endl;

    if(FibreVisu.saveToVTK){
        writeVtkFile(FibreVisu.saveVector, FibreVisu.saveEigenvalues, FibreVisu.saveGrayLevel, FibreVisu.xSize, FibreVisu.ySize, FibreVisu.zSize, name.str().c_str());
    }

    time_t finalTimer;
    time(&finalTimer);
    double elapsed_secs=(double)difftime(finalTimer,timer);

    cout << "Volume "+ FibreVisu.volumeCompleteName+" analyzed and saved in : " << elapsed_secs << " seconds (" << (elapsed_secs/60) << " minutes)" << endl;

    return 1;
}
