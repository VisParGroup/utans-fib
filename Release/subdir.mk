################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../GaussianFiltering.cpp \
../HessianAnalysis.cpp \
../NhdrLoader.cpp \
../PeeksMapsAnalysis.cpp \
../VtkWriter.cpp 

OBJS += \
./GaussianFiltering.o \
./HessianAnalysis.o \
./NhdrLoader.o \
./PeeksMapsAnalysis.o \
./VtkWriter.o 

CPP_DEPS += \
./GaussianFiltering.d \
./HessianAnalysis.d \
./NhdrLoader.d \
./PeeksMapsAnalysis.d \
./VtkWriter.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


