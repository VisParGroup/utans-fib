/*
 * VtkWriter.h
 *
 *  Created on: Jan 30, 2014
 *      Author: hogan
 */

#include <iostream>
#include <utility>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <fstream>
#include </usr/include/eigen3/Eigen/Dense>
#include "/usr/include/eigen3/Eigen/Eigenvalues"

using namespace std;
using namespace Eigen;

#ifndef VTKWRITER_H_
#define VTKWRITER_H_


typedef pair<double[3],Vector3d> coordVector;

//struct Index3 {
//    short i[3];
//};

struct peekAnalysisData{
    int regionIndex;
    int clusteringLevel;
    float centroid[3];
    vector<Vector3d> alignVectors;
};

void writeVtkRegionPeeks(vector<peekAnalysisData> data, const char* fileName);
void writeVtkAlignFile(vector<pair<Vector3d, Vector3d > >& centroids, const char* fileName);
void writeVtkFile(vector<coordVector>& eigenvectors, vector<double>& eigenvalues,vector<double>& graylevel, int sizeX, int sizeY, int sizeZ, const char* filename);
//void LabelMapReader(vector<vector<Index3> >& labelIndices, string fileName, short start[]);

#endif /* VTKWRITER_H_ */
