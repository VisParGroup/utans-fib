
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <fstream>

using namespace std;



//read the string containing the sizes of the .raw file and loads them as int into sizes[]
void splitString(string myString, int VolumeSizes[]){


  size_t found1,found2,found3;
  found1=myString.find(' ');
  VolumeSizes[0]=atoi(myString.substr(0,found1).c_str());
  found2=myString.find(' ',found1+1);
  VolumeSizes[1]=atoi(myString.substr(found1+1,found2-1).c_str());
  found3=myString.find(' ',found2+1);
  VolumeSizes[2]=atoi(myString.substr(found2+1,found3-1).c_str());

//  cout << "SIZES : " << sizes[0] << "|" << sizes[1] << "|" << sizes[2] << endl;
}


//name of the raw file
string file;
//header and new header names
//string headerName;
//line vector used to save the values read from the file
//vector<short> myVec(0);




//read the .nhdr file and extracts the informations regarding the raw file related (sizes and name)
void readNhdr(char* myFile, int Volumesizes[]){
  //_________________variables
  ifstream header;
  string line;
  string subline;
  //string vector containing the .nhdr file data
  vector<string> headerData(0);
  //type of the raw file values
  string typeEl;
  //type of the converted elements
  string newTypeEl;
  //endianness of the data
  string endian;

  header.open(myFile);

  //open header file to read it
  if(!header.good()){
    cout<<"The chosen file does not exist - Software aborted\n";
    exit(0);
  }

// TODO delete if no longer needed  headerName=myFile;

  /// read in every line until the end
  while(header.good()){
    getline(header,line);
    // only store lines that are not comments
    if(line.substr(0,1)!="#"){
      headerData.push_back(line);
    }

    // iterator
    vector<string>::iterator hIt;

    // iterate through all read in lines now stored in headerdata
    for(hIt=headerData.begin();hIt<headerData.end();hIt++){
      line=*hIt;
      // if type is found: store line
      if(line.find("type: ")!= string::npos){
    typeEl = line.substr(6,line.length()-1);
      }
      //identify sizes (x,y,z) and store them
      if(line.find("sizes: ")!= string::npos){
    subline = line.substr(7,line.length()-1);
    splitString(subline, Volumesizes);
      }
      //identify file name
      if(line.find("data file: ")!= string::npos){
    file = line.substr(11,line.length()-1);
      }
      // type of endian used
      if(line.find("endian: ")!= string::npos){
    endian = line.substr(8,line.length()-1);
      }
    }
  }

  //close header file and return to main
  header.close();



}

int countMyElements;

void readRaw(const char *myFile, vector<short>& data){
    //temporary store for the read value
    short cVal;

    FILE *inFile;
    inFile=fopen(myFile,"r");

    while(!feof(inFile)&&!(ferror(inFile))){
        countMyElements+=fread(&cVal,2,1,inFile);
        data.push_back(cVal);
    }

    data.pop_back();
}



//float volume[795][788][576];
//float valueError;
